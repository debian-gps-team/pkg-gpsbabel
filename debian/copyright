Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/gpsbabel/gpsbabel
Files-Excluded: deprecated/gui/serial_mac.cc
                mac
                shapelib
                tools/uploadtool
                zlib
Comment: serial_mac, mac, uploadtool: unused
         shapelib, zlib: packaged in Debian

Files: *
Copyright: 2002, Alex Mottram <geo_alexm@cox-internet.com>
           2008, Alexander Stapff <a.stapff@gmx.de>
           2008, Andreas Grimme <andreas.grimme@gmx.net>
           2006, Andy Armstrong
           2008, Björn Augustsson <oggust@gmail.com>
           2004, Chris Jones
           2009, Chris Tracy <gpsbabel@adiemus.org>
           2006, Curtis E. Mills <archer@eskimo.com>
           2008, Dr. Jrgen Neumann <Juergen.Neumann@online.de>
           2008, Dustin Johnson <Dustin@Dustinj.us>
           2009, Erez Zuler
           2010, Eriks Zelenka <isindir@users.sourceforge.net>
           2005-2006, Etienne Tasse <etasse@yahoo.com>
           2011, Fernando Arbeiza <fernando.arbeiza@gmail.com>
           2005, Fredie Kern <f.kern@xdesy.de>
           2014, Gleb Smirnoff, glebius @t FreeBSD d.t org
           2012, Guilhem Bonnefille <guilhem.bonnefille@gmail.com>
           2007, Gunar Megger <0xff@quantentunnel.de>
           2005, Gustavo Niemeyer <gustavo@niemeyer.net>
           2014, Jean-Claude Repetto <gpsbabel@repetto.org>
           2007, Jeremy Ehrhardt <jeremye@caltech.edu>
           2012, Jeremy Mortis <mortis@tansay.ca>
           2014, Jim Keeler <James.L.Keeler@gmail.com>
           2002, Jochen Becker <jb@bepo.com>
           2003, Mark Bradley <mrcb.gpsb@osps.net>
           2019, Martin Buck <mb-tmp-tvguho.pbz@gromit.dyndns.org>
           2008-2019, Mathias Adam <m.adam@adamis.de>
           2010, Michael von Glasow, michael @t vonglasow d.t com
           2007, Mirko Parthey <mirko.parthey@informatik.tu-chemnitz.de>
           2012, Nicolas Boullis <nboullis@debian.org>
           2005-2008, Olaf Klein <o.b.klein@gpsbabel.org>
           2011, Paul Brook <paul@nowt.org>
           2009, Paul Cornett <pc-gpsb@bullseye.com>
           1998, Paul J. Lucas
           2002, Paul Tomblin <ptomblin@xcski.com>
           2007, Per Borgentun <e4borgen@yahoo.com>
           2020, Pierre Bernard <pierre.bernard@houdah.com>
           2014-2020, Ralf Horstmann <ralf@ackstorm.de>
           2003, Rick Richardson <rickr@mn.rr.com>
           2001-2024, Robert Lipe <robertlipe+source@gpsbabel.org>
           2008, Rodney Lorrimar <rodney@rodney.id.au>
           2003-2014, Ronald L. Parker <ron@parkrrrr.com>
           2004, Scott Brynen <scott@brynen.com>
           2005, Steve Chamberlin <slc@alum.mit.edu>
           2009, Tal Benavidor
           2009, Tobias Kretschmar <tobias.kretschmar@gmx.de>
           2005, Tobias Minich
           2007, Tom Hughes <tom@compton.nu>
           2023, Tyler MacDonald <tyler@macdonald.name>
           2014, Zingo Andersen <zingo@vectrace.com>
           2012-2016, Zingo Andersen <zingo@zingo.org>
           2009, jekaeff
           2013, xiao jian cheng <azuresky.xjc@gmail.com>
           2007, Mark McClure
           2009, S. Khai Mong <khai@mangrai.com>
License: GPL-2+

Files: jeeps/gpsapp.cc
       jeeps/gpscom.cc
       jeeps/gpsfmt.cc
       jeeps/gpsmath.cc
       jeeps/gpsmem.cc
       jeeps/gpsproj.cc
       jeeps/gpsprot.cc
       jeeps/gpsread.cc
       jeeps/gpsrqst.cc
       jeeps/gpssend.cc
       jeeps/gpsserial.cc
       jeeps/gpsutil.cc
Copyright: 2007, Achim Schumacher
           1999-2000, Alan Bleasby
           2010, Martin Buck
           2004-2006, Robert Lipe
License: GPL-2+

Files: strptime/strptime.h
Copyright: 1989-1991, Free Software Foundation, Inc
           1998, Gerald Combs
License: GPL-2+

Files: strptime/strptime_l.c
Copyright: 1991-2023, Free Software Foundation, Inc
License: LGPL-2.1+

Files: deprecated/queue.h
Copyright: 2002-2005, Robert Lipe <robertlipe@usa.net>
           2001, Simon Tatham
License: Expat or GPL-2+

Files: debian/*
License: GPL-2+
Copyright: 2004-2008, John Goerzen <jgoerzen@complete.org>
           208-2017, Bernd Zeimetz <bernd@bzed.de>
           2019, Jochen Sprickerhof <jspricke@debian.org>

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
  On Debian GNU/Linux systems, the complete text of the GNU General
  Public License version 2 can be found in
  `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2 can be found in /usr/share/common-licenses/LGPL-2.1.
