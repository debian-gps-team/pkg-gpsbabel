gpsbabel (1.10.0+ds-1) unstable; urgency=medium

  * New upstream version
  * Drop qtci from Files-Excluded (dropped upstream)
  * Update d/copyright
  * Update d/patches
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 11 Jan 2025 08:01:10 +0100

gpsbabel (1.9.0+ds-3) unstable; urgency=medium

  * Depend on locales-all for tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 08 Jun 2024 14:07:37 +0200

gpsbabel (1.9.0+ds-2) unstable; urgency=medium

  * Fix man page generation
  * Fix build on 32bit archs

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 24 Oct 2023 16:08:38 +0200

gpsbabel (1.9.0+ds-1) unstable; urgency=medium

  * New upstream version
  * Update d/copyright
  * Rebase patches
  * Switch build system to cmake
  * Update d/clean (Closes: #1044858)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 23 Oct 2023 11:27:32 +0200

gpsbabel (1.8.0+ds-7) unstable; urgency=medium

  * Fix package short description (Closes: #1042359)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 27 Jul 2023 11:10:25 +0200

gpsbabel (1.8.0+ds-6) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Don't use qtwebengine5-dev on mipsel, package to be removed
    (Closes: #1041912)

  [ Jochen Sprickerhof ]
  * Bump policy version (no changes)
  * Move package description to source package

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 25 Jul 2023 11:47:33 +0200

gpsbabel (1.8.0+ds-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Jochen Sprickerhof ]
  * Add upstream source
  * Add workaround for failing lconvert (Closes: #1013610)
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 25 Jun 2022 21:49:08 +0200

gpsbabel (1.8.0+ds-4) unstable; urgency=medium

  * Drop gmapbase.html (embedded in gpsbabelfe)
  * Fix HTML documentation.
    Thanks to tsteven4

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 25 Jan 2022 23:19:17 +0100

gpsbabel (1.8.0+ds-3) unstable; urgency=medium

  * Add qttranslations5-l10n to -gui
  * Add support for Hurd

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 24 Jan 2022 16:09:49 +0100

gpsbabel (1.8.0+ds-2) unstable; urgency=medium

  * Add patch for failing test on i386
  * Use Debian build flags for the GUI

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 23 Jan 2022 22:24:45 +0100

gpsbabel (1.8.0+ds-1) unstable; urgency=medium

  * Update Files-Excluded for new upstream version
  * New upstream version
  * Rebase patches
  * Update copyright
  * Port to qmake
  * Bump policy versions (no changes)
  * Update build systems of gpsbabel-gui
  * Update supported formats
  * Drop trailing whitespace to please lintian
  * Add missing build dependency
  * Support translated gui
  * Add patch for donation nagging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 23 Jan 2022 21:25:59 +0100

gpsbabel (1.7.0+ds-7) unstable; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Remove qt5-default from build dependencies (Closes: #972164).
    - Replace it with qtbase5-dev, which is the appropriate one.
    - Export QT_SELECT := qt5 to let qtchooser use the right Qt version.

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 17 Oct 2020 12:37:40 +0200

gpsbabel (1.7.0+ds-6) unstable; urgency=medium

  * Set TZ for tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 13 Oct 2020 22:39:49 +0200

gpsbabel (1.7.0+ds-5) unstable; urgency=medium

  * Add patch for i386 test failure

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 07 Aug 2020 15:38:18 +0200

gpsbabel (1.7.0+ds-4) unstable; urgency=medium

  * Add patch for endianness issues

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 07 Aug 2020 11:31:04 +0200

gpsbabel (1.7.0+ds-3) unstable; urgency=medium

  * Fix test logic

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 10 Jul 2020 16:13:04 +0200

gpsbabel (1.7.0+ds-2) unstable; urgency=medium

  * Update arch list that ignore test result

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 10 Jul 2020 15:09:25 +0200

gpsbabel (1.7.0+ds-1) unstable; urgency=medium

  * New upstream version
  * Update file excludes
  * Update d/copyright
  * Simplify d/watch
  * rebase patches
  * bump debhelper version (no changes)
  * Depend on libusb-1.0-0-dev (Closes: #810420)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 09 Jul 2020 21:54:33 +0200

gpsbabel (1.6.0+ds-11) unstable; urgency=medium

  * Add patch for strcmp (Closes: #957304)
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 21 Apr 2020 19:40:57 +0200

gpsbabel (1.6.0+ds-10) unstable; urgency=medium

  * [a1fdee8] Install gmapbase.html.
    Also patch a useful path into gpsbabelfe. (LP: #1015886)

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 22 Dec 2019 00:27:08 +0100

gpsbabel (1.6.0+ds-9) unstable; urgency=medium

  * [a1fdee8] Install gmapbase.html.
    Also patch a useful path into gpsbabelfe. (LP: #1015886)

 -- Bernd Zeimetz <bzed@debian.org>  Sat, 21 Dec 2019 23:45:28 +0100

gpsbabel (1.6.0+ds-8) unstable; urgency=medium

  * Add patch for "super gps"
    Thanks to Tim Connors (Closes: #935373)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 24 Sep 2019 22:09:09 +0200

gpsbabel (1.6.0+ds-7) unstable; urgency=medium

  * Drop old build dependency
  * Fix build depends

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 03 Sep 2019 23:46:06 +0200

gpsbabel (1.6.0+ds-6) unstable; urgency=medium

  * Use upstream manpage
    (Closes: #522332, #618856, #803509, #803512, #698326, #862299)
  * Drop note about quilt
  * Add patch for cross compilation.
    Thanks to Helmut Grohne (Closes: #934849)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 03 Sep 2019 21:28:10 +0200

gpsbabel (1.6.0+ds-5) unstable; urgency=medium

  * Only override build-arch in d/rules, fixes building on all

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 26 Jul 2019 15:54:26 +0200

gpsbabel (1.6.0+ds-4) unstable; urgency=medium

  * Fix gpsbabel-gui package (Closes: #932875)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 26 Jul 2019 11:12:00 +0200

gpsbabel (1.6.0+ds-3) unstable; urgency=medium

  * Build pdf on mips again as #580333 was fixed
  * Ignore test results on some architectures

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 19 Jul 2019 14:08:44 +0200

gpsbabel (1.6.0+ds-2) unstable; urgency=medium

  * Build doc in build-indep

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 19 Jul 2019 00:55:34 +0200

gpsbabel (1.6.0+ds-1) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Cleanup upstream sources
  * New upstream version (Closes: #805355)
  * Fix debian/source/format
  * Add watch file
  * Rebase patches
  * Add patches to use system libraries
  * Port to dh 12 (Closes: #912089)
  * Update copyright

  [ Bernd Zeimetz ]
  * Use gitlab-ci instead of travis
  * Update Vcs-Git Urls.

  [ Jochen Sprickerhof ]
  * Maintainer set to team and add myself as uploader
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 18 Jul 2019 23:32:20 +0200

gpsbabel (1.5.4-2) unstable; urgency=medium

  * [cc58f71] Move git repository to github.
  * [fb44d98] Add a debian .travis.yml
  * [28c7ad3] Don't build the gui on [!amd64 arm64 armhf i386 mipsel]
    The recent qtwebengine is not available on all architectures.
    Thanks to Adrian Bunk (Closes: #877808)

 -- Bernd Zeimetz <bzed@debian.org>  Sat, 21 Oct 2017 13:37:08 +0200

gpsbabel (1.5.4-1) unstable; urgency=medium

  * [ee23f26] Merge branch 'upstream'
    - New upstream version.
    - Builds fine with gcc7.
    Thanks to Adrian Bunk (Closes: #872744)
  * [f340fb9] Refreshing patches.
  * [8100370] export QT_SELECT=qt5
  * [522c580] Build for/with QT5.
    Using as build-deps:
    qtbase5-dev
    qtwebengine5-dev
    qttools5-dev-tools
    Thanks to Dmitry Shachnev (Closes: #816808)
  * [7d38162] Depend on dh 10 for comapt level 10.
  * [ada8fa9] Fix building with recent QT5 versions.
  * [d627435] Use qmake instead of qmake-qt4.
  * [c08a568] Updating changelog.

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 28 Aug 2017 23:53:53 +0200

gpsbabel (1.5.3-2) unstable; urgency=medium

  * [b2954b1] Fix skytraq default values.
    Taken from upstream
    Thanks to Jeroen N. Witmond (Closes: #846945)

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 01 Jan 2017 22:06:31 +0100

gpsbabel (1.5.3-1) unstable; urgency=medium

  * [41ba336] Merge branch 'upstream'
  * [9511cf2] removing extra gpsbabel directory from d/rules.
  * [65959cd] Refreshing patches.
  * [cd70dc1] ensur emissing minizip .o files are built
  * [48328e9] Fix help2man call for gpsbabel
  * [bf2c668] Bump dh compat level to 10.
  * [ec910a4] Remove gpsbabel/ from some more places.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 27 Nov 2016 16:18:18 +0100

gpsbabel (1.5.2-1) unstable; urgency=medium

  * [fda6f38] Merge branch 'upstream'
  * [901aac0] Refreshing patches.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 25 Oct 2015 20:36:55 +0100

gpsbabel (1.5.0-3) unstable; urgency=medium

  * [5b27c1cd] Install gpsbabel.html only.
    Remove php-ized htmldoc folder.
    The whole documentation is available as pdf anyway.
    Thanks to Axel Beckert (Closes: #755457)

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 04 Aug 2014 16:23:33 +0200

gpsbabel (1.5.0-2) unstable; urgency=medium

  * [d7bbdd34] Add menu and desktop files for gpsbabel-gui.
    Thanks to Julien Valroff (Closes: #592799)
  * [c8c9fc34] Ignore make check results for now.
    Checks fail on various architectures due to endianess/byte ordering
    issues or due to floating point numbers not being completely equal.
  * [bd0b02a9] Build-depend on locales-all.
    make check needs the en_US.UTF8 locale.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 14 May 2014 22:19:45 +0200

gpsbabel (1.5.0-1) unstable; urgency=medium

  * [72ac73df] Support nocheck in DEB_BUILD_OPTIONS.
  * [3eb772af] Importing upstream version 1.4.4
  * [91e94593] Refreshing patches.
  * [d9a07846] Updating changelog.
  * [e089d5ab] Merging upstream 1.5.0
  * [77258acf] Refreshing patches
  * [cba6d75d] Create/clean autogen directory.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 13 May 2014 01:19:24 +0200

gpsbabel (1.4.3-1) unstable; urgency=low

  * New upstream release (Closes: #660476)
  * [0754cb61] Merge branch 'upstream'
  * [0dc59f07] Merge branch 'upstream'
  * [337703bb] Refreshing patches.
  * [6e23bc91] Migrate debian/rules and .install files to gpsbabel subdirectory.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 16 May 2012 23:28:46 +0200

gpsbabel (1.4.2-3) unstable; urgency=low

  * [64bae20c] Use libqtwebkit-dev as build-dependency. (Closes: #629742)

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 04 Jul 2011 16:17:13 +0200

gpsbabel (1.4.2-2) unstable; urgency=low

  * [8251cb18] Work around the issues of the sbuild internal resolver.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 15 Feb 2011 11:12:09 +0100

gpsbabel (1.4.2-1) unstable; urgency=low

  * [15f28bac] gpsbabel-gui needs to depend on gpsbabel.
    Add the missing dependency.
  * [e5c3f560] Build-depend on libqtwebkit-dev for recent QT versions.
  * [709bd4d9] Merge branch 'upstream'
  * [f0e46422] Refreshing patches for new upstream version.

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 14 Feb 2011 14:14:26 +0100

gpsbabel (1.4.0-1) unstable; urgency=low

  * New upstream version.
  * [37a299e3] Merge branch 'upstream'
  * [01923dca] Removing limited_pointer_type_comparison_fixes patch,
    applied upstream.
  * [4d4ea8f8] Refreshing patches.
  * [f4ef9d52] Add various patches to integrate gpsbabelfe better into
    Debian.
    - Submitting statistics data should be opt-in instead of
      activated by default
    - Disable check for newer versions. apt-get/aptitude takes care of that.
    - Disable check for version differences between gpsbabelfe and gpsbabel.
      The check did not work as expected and dpkg ensures there is
      no difference.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 23 Jun 2010 01:44:11 +0200

gpsbabel (1.3.7~cvs2-3) unstable; urgency=low

  * [b76106eb] Rename gpsbabel.doc-base parts to stop confusing
    dh_installdocs.
  * [0c9397ce] Split documentation into gpsbabel-doc.
  * [08e7b6ec] Fix build target dependency for build-doc.

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 06 May 2010 12:12:41 +0200

gpsbabel (1.3.7~cvs2-2) unstable; urgency=low

  * [014dfaaa] Use $(QUILT_STAMPFN) instead of patch as dependency. This
    avoids running configure twice.
  * [75bd04e0] Require make check to succeed for a successful build.
  * [fe9c247b] Better dependencies for the install target.
  * [9a028647] Remove build-dependencies on texlive. The tex based
    documentation is not being built anymore.
  * [182c909d] Fix various comparison errors due to limited pointer
    types.
  * [f6d8e680] Use -ffloat-store on i386.
  * [760285e1] Do not build gpsbabel.pdf on mips as fop is broken there.

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 06 May 2010 00:31:46 +0200

gpsbabel (1.3.7~cvs2-1) unstable; urgency=low

  * Updating to CVS HEAD of today.
  * [09ce2bac] Build pdf file again, for works fine now.
  * [3542f981] Require dh 7 for compat level 7.
  * [5f340180] Do some housekeeping in debian/rules.
  * [fd477016] Merge branch 'upstream'
  * [5f1d5231] Clean gui/setup.iss after build.
  * [56dd1e9b] Rename htmldoc directory and change the references to it.
  * [05194507] Install docs only into the gpsbabel package.
  * [e932db97] Build a manpage for gpsbabel using help2man.
  * [dd121ff7] Merge remote branch 'origin/upstream-cvs'
  * [475d34c2] gpsbabel is reuired to build the manpage. Add a proper
    dependency to the target.
  * [74b24bde] Don't call clean in doc, not built anymore.
  * [90bb4cc5] Add AUTHORS file to docs.
  * [a807b146] Install gpsbabel.pdf to /usr/share/doc/gpsbabel.
  * [d65fa27d] Move doc production into its own target.
  * [8e6e25a7] Delete all automatically generated and left over files.
  * [851f5a32] Use source format 1.0 for easy backporting.
  * [788292c2] Bump Standards-Version to 3.8.4 - no changes needed.
  * [db9f500a] Add doc-base information for gpsbabel.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 05 May 2010 00:25:48 +0200

gpsbabel (1.3.7~cvs1-1) unstable; urgency=low

  * Updating to CVS HEAD of today.
  * [4d21a068] Drop Debian's gpsbabel manapge.
  * [a350b9f6] Update patches for current gpsbabel beta.
  * [0516a23a] Bump debhelper compat level to 7.
  * [f964f0e2] Do no require make check to pass for a successful build.
  * [f2f46177] Add install file for gpsbabel gui.
  * [1ce3dba8] Don't call makeextras.mak, the file is gone.
  * [f590f252] Add patch to rename the gpsbabelfe binary.
  * [e686c93a] Add fop as build-dependency to build the documentation.
  * [1bcda881] Update debian/copyright file.

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 26 Apr 2010 14:24:21 +0200

gpsbabel (1.3.6+cvs1-1) experimental; urgency=low

  * Updating to latest CVS head
    (c0af31ca669e2ea66ad33a27f23f0efffef2c948).

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 07 Apr 2009 18:06:49 +0200

gpsbabel (1.3.6-3) unstable; urgency=low

  * debian/patches:
    - Adding 'osm-char-int-cast-bug-fix' patch to remove an unneeded
      cast from int to char and back to int which resulted in a broken
      osm module on some architectures.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 07 Jan 2009 16:12:17 +0100

gpsbabel (1.3.6-2) unstable; urgency=low

  * debian/rules:
    - Enable 'make doc' again, it was accidentally left disabled.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 07 Jan 2009 00:05:46 +0100

gpsbabel (1.3.6-1) unstable; urgency=low

  * Imported Upstream version 1.3.6
  * Dropping Debian specific patches which were applied to the source
    directly. They should not be needed anymore.
  * debian/rules:
     - Cleaning up unused lines.
     - Don't ignore make check failures.
  * debian/control:
     - Setting myself as maintainer in agreement with John Goerzen.
       Thanks to John for maintaining the package until now!
     - Updating Vcs fields to reflect the new location of the git
       repository.
     - Bumping Standards-Version to 3.8.0 - no changes needed.
     - Adding libpaper-dev as Build-Dependency as an appropriate
       dependency is missing in docbook-xsl.
     - Adding quilt Build-Dep to handle patches.
  * debian/dirs: dropping file, not needed anymore.
  * debian/patches:
    - Adding patch to stop building gpsbabel.pdf as fop is not in main.
  * debian/README.source:
    - Adding file to describe the use of quilt.
  * debian/watch:
    - Dropping file, author doesn't provide useful download pages anymore.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 06 Jan 2009 16:12:28 +0100

gpsbabel (1.3.5-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fixing FTBFS (Closes: #491364)
  * debian/control:
    - adding autoconf and autotools-dev as build dependencies as
      Package falied to build from source (Closes: #491364)
  * debian/rules:
    - Copying config.sub/guess from autotools-dev
    - Don't ignore errors on clean, fixing a lintian warning.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 20 Jul 2008 21:36:20 +0200

gpsbabel (1.3.5-1) unstable; urgency=low

  * New upstream release.  Closes: #480449.

 -- John Goerzen <jgoerzen@complete.org>  Wed, 16 Jul 2008 13:04:50 -0500

gpsbabel (1.3.4-2) unstable; urgency=low

  * Add Homepage and Vcs-* lines to Debian/control.

 -- John Goerzen <jgoerzen@complete.org>  Sun, 02 Mar 2008 08:52:21 -0600

gpsbabel (1.3.4-1) unstable; urgency=low

  * New upstream release.  Closes: #459234.  Upstream has dropped
    Coldsync.  Closes: #421769.
  * Update debian/copyright.
  * Add garmin_usb info to README.Debian.  Closes: #344702.

 -- John Goerzen <jgoerzen@complete.org>  Thu, 14 Feb 2008 05:50:31 -0600

gpsbabel (1.3.3-2) unstable; urgency=low

  * Added build-dep on texlive-base.  Closes: #427620.
  * Fixed path of binary.  Closes: #427365.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 05 Jun 2007 01:48:37 -0500

gpsbabel (1.3.3-1) unstable; urgency=low

  * New upstream release.  Closes: #330416, #419884.
  * Added manpage for Joaquim Ortega-Cerda.  Closes: #342324.
  * Fixed typo in debian/copyright.  Closes: #421769.

 -- John Goerzen <jgoerzen@complete.org>  Fri, 01 Jun 2007 02:28:54 -0500

gpsbabel (1.3.2-2) unstable; urgency=high

  * Fix doc generation.  Closes: #403092.

 -- John Goerzen <jgoerzen@complete.org>  Mon, 18 Dec 2006 05:53:16 -0600

gpsbabel (1.3.2-1) unstable; urgency=low

  * New upstream version.  Closes: #383899, #380533, #304178.
  * Style docs now included.  Closes: #348986.  (Note: README.style
    no longer exists)

 -- John Goerzen <jgoerzen@complete.org>  Fri, 17 Nov 2006 03:57:30 -0600

gpsbabel (1.2.7-1) unstable; urgency=low

  * New upstream version.  Closes: #317559.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 11 Oct 2005 05:31:39 -0500

gpsbabel (1.2.5+1.2.6-beta20050608-1) unstable; urgency=low

  * New usptream version.  Closes: #315227.
  * New version includes support for USB.  Closes: #315134.
  * New upstream version already has patch for my_rd_deinit.
    Closes: #315164.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 21 Jun 2005 05:59:58 -0500

gpsbabel (1.2.4-2) unstable; urgency=low

  * make check failure no longer causes build failure.  There are apparently
    a lot of minor differences in rounding on different archs that were
    causing this.  Closes: #294717.

 -- John Goerzen <jgoerzen@complete.org>  Mon, 14 Mar 2005 10:39:57 -0600

gpsbabel (1.2.4-1) unstable; urgency=low

  * New upstream release.  Closes: #291544.

 -- John Goerzen <jgoerzen@complete.org>  Thu, 27 Jan 2005 11:08:20 -0600

gpsbabel (1.2.3-1) unstable; urgency=low

  * Initial Release.  Closes: #270812.

 -- John Goerzen <jgoerzen@complete.org>  Thu,  9 Sep 2004 08:14:00 -0500
